﻿using Krem.AppCore.Attributes;
using UnityEngine;

namespace Krem.AppCore.Basis.Actions
{
    [NodeGraphGroupName("Jet Pack/Basis")]
    public class ConsoleLog : CoreAction
    {
        [ActionParameter] public string ConsoleText = "";

        protected override bool Action()
        {
            Debug.Log(ConsoleText);
            
            return true;
        }
    }
}
