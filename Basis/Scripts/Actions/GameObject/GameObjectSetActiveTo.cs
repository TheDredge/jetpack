using Krem.AppCore.Attributes;
using UnityEngine;

namespace Krem.AppCore.Basis.Actions
{
    [NodeGraphGroupName("Jet Pack/Basis/GameObject")] 
    public class GameObjectSetActiveTo : CoreAction 
    {
        [InjectComponent] private Transform _transform;
        
        [ActionParameter] public bool state = false;
        
        protected override bool Action()
        {
            _transform.gameObject.SetActive(state);
        
            return true;
        }
    }
}