using Krem.AppCore.Attributes;
using Krem.AppCore.Basis.Components.Links;
using Krem.AppCore.Ports;
using UnityEngine;

namespace Krem.AppCore.Basis.Actions
{
    [NodeGraphGroupName("Jet Pack/Basis/Rigidbody")] 
    public class SetRigidbodyIsKinematicTo : CoreAction
    {
        [InjectComponent] private Rigidbody _rigidbody;

        [ActionParameter] public bool isKinematic = true;
        
        protected override bool Action()
        {
            _rigidbody.isKinematic = isKinematic;
        
            return true;
        }
    }
}