using Krem.AppCore.Attributes;
using UnityEngine;

namespace Krem.AppCore.Basis.Components.Handlers
{
    [NodeGraphGroupName("Jet Pack/Basis/Handlers")]
    public class LateUpdateHandler : BaseUpdateHandler
    {
        private void LateUpdate()
        {
            DeltaTime.Data = Time.deltaTime;
            
            OnUpdate.Invoke();
        }
    }
}