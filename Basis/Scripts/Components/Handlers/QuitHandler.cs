using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace Krem.AppCore.Basis.Components.Handlers
{
    [NodeGraphGroupName("Jet Pack/Basis/Handlers")]
    public class QuitHandler : CoreComponent
    {
        [Header("Ports")]
        public OutputSignal OnQuit;

        private void OnApplicationQuit()
        {
            OnQuit.Invoke();
        }
    }
}