using System.Diagnostics.CodeAnalysis;
using Krem.AppCore.Attributes;
using UnityEngine;
using UnityEngine.Rendering;

namespace Krem.AppCore.Basis.Components.Links
{
    [NodeGraphGroupName("Jet Pack/Basis/Links")]
    [DisallowMultipleComponent]
    public sealed class VolumeLink : CoreComponent
    {   
        [Header("Dependencies")]
        [SerializeField, NotNull] protected Volume _volume;

        [Header("States")]
        public bool active = false;

        public Volume Volume => _volume;
    }
}