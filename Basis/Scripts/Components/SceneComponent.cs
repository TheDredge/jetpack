using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace Krem.AppCore.Basis.Components
{
    [NodeGraphGroupName("Jet Pack/Basis")]
    public class SceneComponent : CoreComponent
    {    
        [Header("Data")]
        public string sceneName;
        
        [Header("Settings")]
        public bool reloadCurrentScene = false;

        [Header("Ports")]
        public OutputSignal RequestLoading;

        public void RequestLoadScene()
        {
            RequestLoading.Invoke();
        }
    }
}