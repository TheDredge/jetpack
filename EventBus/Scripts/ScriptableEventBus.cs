using System;
using UnityEngine;

namespace Krem.AppCore.EventBus
{
    [CreateAssetMenu(fileName = "ScriptableEventBus", menuName = "AppCore/EventBus")]
    public class ScriptableEventBus : ScriptableObject
    {
        protected event Action _event;

        public virtual void Invoke()
        {
            _event?.Invoke();
        }
        
        public virtual void AddListener(Action listener)
        {
            _event += listener;
        }

        public virtual void RemoveListener(Action listener)
        {
            _event -= listener;
        }
    }
}
