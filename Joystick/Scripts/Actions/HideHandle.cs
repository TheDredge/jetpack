using Krem.AppCore.Attributes;

namespace Krem.AppCore.Joystick.Actions
{
    [NodeGraphGroupName("Jet Pack/Joystick")]
    public class HideHandle : CoreAction
    {
        [InjectComponent] private Components.Joystick _joystick;
        
        protected override bool Action()
        {
            _joystick.Handle.gameObject.SetActive(false);
        
            return true;
        }
    }
}