using Krem.AppCore.Attributes;
using Krem.AppCore.Basis.Components.Links;
using Krem.AppCore.Joystick.Components;
using UnityEngine;

namespace Krem.AppCore.Joystick.Actions
{
    [NodeGraphGroupName("Jet Pack/Joystick/Movables")]
    public class TransformMove : CoreAction
    {
        [InjectComponent] private TransformMovable _transformMovable;
        [InjectComponent] private TransformLink _transformComponent;

        private Vector3 _moveVector = Vector3.zero;
        
        protected override bool Action()
        {
            _moveVector.x = _transformMovable.InputAxis.Axis.x * _transformMovable.sensitivity * Time.deltaTime;
            _moveVector.z = _transformMovable.InputAxis.Axis.y * _transformMovable.sensitivity * Time.deltaTime;
            _moveVector.y = 0;
            
            _transformComponent.Transform.Translate(_moveVector);
        
            return true;
        }
    }
}