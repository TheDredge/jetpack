﻿using Krem.AppCore.Attributes;
using UnityEngine;

namespace Krem.AppCore.Joystick.Actions
{
    [NodeGraphGroupName("Jet Pack/Joystick")]
    public class PointerUp : CoreAction
    {
        [InjectComponent] private Components.Joystick _joystick;

        protected override bool Action()
        {
            _joystick.Handle.anchoredPosition = _joystick.Body.anchoredPosition;
            
            _joystick.Axis = Vector2.zero;

            return true;
        }
    }
}