using System.Diagnostics.CodeAnalysis;
using Krem.AppCore.Attributes;
using Krem.AppCore.Joystick.Interfaces;
using UnityEngine;

namespace Krem.AppCore.Joystick.Components
{
    [NodeGraphGroupName("Jet Pack/Joystick")]
    [DisallowMultipleComponent]
    public class RigidbodyMovable : CoreComponent
    {
        [Header("Dependencies")]
        [SerializeField, NotNull] protected Rigidbody _rigidbody;
        [SerializeField, NotNull] protected Axis2D _inputAxis;

        [Header("Settings")]
        public float sensitivity = 2f;

        public Rigidbody Rigidbody => _rigidbody;
        public IAxis2D InputAxis => _inputAxis;
    }
}