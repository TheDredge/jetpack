using Krem.AppCore.Joystick.Interfaces;
using UnityEngine;

namespace Krem.AppCore.Joystick.Scriptables
{
    [CreateAssetMenu(fileName = "ScriptableAxis2D", menuName = "Jet Pack/Joystick/ScriptableAxis2D", order = 0)]
    public class ScriptableAxis2D : ScriptableObject, IAxis2D
    {
        public Vector2 Axis { get; set; }
    }
}
