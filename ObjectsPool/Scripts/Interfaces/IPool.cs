using System.Collections.Generic;

namespace Krem.AppCore.ObjectsPool.Interfaces
{
    public interface IPool<T> where T : IPoolItem
    {
        public T GetFromPool();
        public void ReturnToPool(T item);
        public Queue<T> PoolQueue { get; }
        public List<T> PoolItems { get; }

        public void InstantiatePool();
        
        public void ExpandPool(int size);
    }
}