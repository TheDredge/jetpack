﻿using System;
using System.Collections.Generic;

namespace Krem.AppCore.ScriptableORM
{
    [Serializable]
    public class Collection<TModel> where TModel : Model
    {
        public List<TModel> Data;
    }
}
