using System.Collections.Generic;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace Krem.AppCore.ScriptableORM.Components
{
    [NodeGraphGroupName("Jet Pack/Scriptable ORM")]
    public abstract class BaseScriptableModelListProvider<TScriptableModel, TModel> : CoreComponent where TModel : Model, new() where TScriptableModel : ScriptableList<TModel>
    {    
        [Header("Dependencies")]
        [SerializeField] protected TScriptableModel _scriptableModel;
        
        [Header("Ports")]
        [BindInputSignal(nameof(Save))] public InputSignal CallSave;
        [BindInputSignal(nameof(Load))] public InputSignal CallLoad;
        [BindInputSignal(nameof(Delete))] public InputSignal CallDelete;
        public OutputSignal OnSaved;
        public OutputSignal OnLoaded;
        public OutputSignal OnDelete;

        public List<TModel> Collection
        {
            get => _scriptableModel.Collection;
            set => _scriptableModel.Collection = value;
        }

        public void Save()
        {
            if (_scriptableModel.Save() == false)
            {
                return;
            }

            OnSaved.Invoke();
        }

        public void Load()
        {
            
            if (_scriptableModel.Load() == false)
            {
                return;
            }
            
            OnLoaded.Invoke();
        }

        public void Delete()
        {
            if (_scriptableModel.Delete() == false)
            {
                return;
            }
            
            OnDelete.Invoke();
        }
    }
}