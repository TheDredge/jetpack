using System.ComponentModel;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace Krem.AppCore.ScriptableORM.Components
{
    [NodeGraphGroupName("Jet Pack/Scriptable ORM")]
    public abstract class BaseScriptableModelProvider<TScriptableModel, TModel> : CoreComponent where TModel : Model, new() where TScriptableModel : ScriptableModel<TModel>
    {    
        [Header("Dependencies")]
        [SerializeField] protected TScriptableModel _scriptableModel;
        
        [Header("Ports")]
        [BindInputSignal(nameof(Save))] public InputSignal CallSave;
        [BindInputSignal(nameof(Load))] public InputSignal CallLoad;
        [BindInputSignal(nameof(Delete))] public InputSignal CallDelete;
        public OutputSignal OnSaved;
        public OutputSignal OnLoaded;
        public OutputSignal OnDelete;
        public OutputSignal OnValueChanged;

        public TModel Model => _scriptableModel.Model;
        
        private void OnEnable()
        {
            _scriptableModel.Model.PropertyChanged += ValueChangedListener;
        }

        private void OnDisable()
        {
            _scriptableModel.Model.PropertyChanged -= ValueChangedListener;
        }

        private void ValueChangedListener(object sender, PropertyChangedEventArgs e)
        {
            OnValueChanged.Invoke();
        }

        public void Save()
        {
            if (_scriptableModel.Save() == false)
            {
                return;
            }

            OnSaved.Invoke();
        }

        public void Load()
        {
            _scriptableModel.Model.PropertyChanged -= ValueChangedListener;
            
            if (_scriptableModel.Load() == false)
            {
                _scriptableModel.Model.PropertyChanged += ValueChangedListener;

                _scriptableModel.Save();
            }
            
            _scriptableModel.Model.PropertyChanged += ValueChangedListener;
            
            OnLoaded.Invoke();
        }
        
        public void Delete()
        {
            if (_scriptableModel.Delete() == false)
            {
                return;
            }
            
            OnDelete.Invoke();
        }
    }
}