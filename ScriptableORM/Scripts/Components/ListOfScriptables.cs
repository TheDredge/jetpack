using System.Collections.Generic;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using Krem.AppCore.ScriptableORM.Interfaces;
using UnityEngine;

namespace Krem.AppCore.ScriptableORM.Components
{
    [NodeGraphGroupName("Jet Pack/Scriptable ORM")]
    [DisallowMultipleComponent]
    public class ListOfScriptables : CoreComponent
    {    
        [Header("Dependencies")]
        [SerializeField] private List<ScriptableObject> repositories = new List<ScriptableObject>();
        
        [Header("Settings")]
        public bool loadOnAwake = false;
        public bool saveOnDestroy = false;
        
        [Header("Ports")]
        [BindInputSignal(nameof(Save))] public InputSignal CallSave;
        [BindInputSignal(nameof(Load))] public InputSignal CallLoad;
        public OutputSignal OnSaved;
        public OutputSignal OnLoaded;
        
        private void Awake()
        {
            if (loadOnAwake)
            {
                Load();
            }
        }

        private void OnDestroy()
        {
            if (saveOnDestroy)
            {
                Save();
            }
        }
        
        public void Save()
        {
            repositories.ForEach(repository =>
            {
                ((IScriptableRepository) repository).Save();
            });
            
            
            OnSaved.Invoke();
        }

        public void Load()
        {
            repositories.ForEach(repository =>
            {
                ((IScriptableRepository) repository).Load();
            });

            OnLoaded.Invoke();
        }
    }
}