using UnityEngine;

namespace Krem.AppCore.ScriptableORM.Models
{
    [CreateAssetMenu(fileName = "StringScriptableList", menuName = "ScriptableORM/StringScriptableList", order = 0)]
    public class StringScriptableList : ScriptableList<StringModel>
    {
    }
}
