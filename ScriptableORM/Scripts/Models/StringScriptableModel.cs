using UnityEngine;

namespace Krem.AppCore.ScriptableORM.Models
{
    [CreateAssetMenu(fileName = "StringScriptableModel", menuName = "ScriptableORM/StringScriptableModel", order = 0)]
    public class StringScriptableModel : ScriptableModel<IntModel> 
    {
    }
}